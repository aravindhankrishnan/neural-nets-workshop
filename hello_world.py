import tensorflow as tf

message = tf.constant('hello world')

with tf.Session() as sess:
  print('Message = ', sess.run(message).decode())
