import tensorflow as tf

x = tf.placeholder('float')
data = tf.random_uniform([1, 10], minval=0, maxval=10)
K = tf.constant(5, dtype=tf.float32, shape=[1, 10])

with tf.Session() as sess:
  x2 = tf.square(x)
  two_x = 2 * x
  result = tf.add(x2, two_x)
  result += K

  data_x = sess.run(data)
  print(sess.run(result, feed_dict={x:data_x}))
