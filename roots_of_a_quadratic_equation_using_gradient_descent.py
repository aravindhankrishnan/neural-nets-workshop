import tensorflow as tf

def main():
  # Quadratic equation x^2 - 4x + 4
  x = tf.Variable(0.0)
  eta = tf.constant(0.1)
  dx = tf.add(tf.multiply(2.0, x), -4.0)
  update = x.assign(x - tf.multiply(eta, dx))
  init = tf.global_variables_initializer()
  with tf.Session() as sess:
    sess.run(init)
    i = 0
    while True:
      px = x.eval()
      sess.run([update])
      i += 1
      print('Iteration = ', i, 'x = ', x.eval())
      if abs(px - x.eval()) < 0.001:
        break
    result = sess.run(x)

  print('Root = ', result)
  print('Value = ', result * result - 4 * result + 4)
  
if __name__ == '__main__':
  main()
