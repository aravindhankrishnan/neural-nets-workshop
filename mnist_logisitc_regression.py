import numpy as np
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

def main():
  mnist = input_data.read_data_sets('MNIST_data/', one_hot=True)

  batch_size = 100
  X = tf.placeholder(tf.float32, [None, 784])
  Y = tf.placeholder(tf.float32, [None, 10])
  ground_truth = tf.placeholder(tf.float32, [10000, 10])

  W = tf.Variable(tf.zeros([784, 10]))
  b = tf.Variable(tf.zeros([10]))

  Y_hat = tf.nn.softmax(tf.matmul(X, W) + b)
  loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=Y, logits=Y_hat))

  optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.1)
  training_step = optimizer.minimize(loss)

  init = tf.global_variables_initializer()

  max_epochs = 10
  num_batches = int(mnist.train.num_examples / batch_size)

  evaluate = tf.multiply(tf.reduce_mean(tf.cast(tf.equal(tf.argmax(Y_hat, axis=1), 
      tf.argmax(ground_truth, axis=1)), tf.float32)), 100.)

  with tf.Session() as sess:
    sess.run(init)

    for epoch in range(max_epochs):
      for _ in range(num_batches):
        data, digits = mnist.train.next_batch(batch_size)
        sess.run([training_step, loss], feed_dict={X:data, Y:digits})
    print('Accuracy = ', sess.run([evaluate], feed_dict={X:mnist.test.images, ground_truth:mnist.test.labels}))

if __name__ == '__main__':
  main()
