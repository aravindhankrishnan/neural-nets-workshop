import numpy as np
import sys
import tensorflow as tf

# Generating ground truth data
data = tf.random.uniform(shape=[1, 10], dtype=tf.float32, minval=0, maxval=1)

w = tf.constant(5.0)
b = tf.constant(3.0)

with tf.Session() as sess:
  x = sess.run(data)
  y = sess.run(tf.add(tf.multiply(w, x), b))

# Doing the regression

tx = tf.placeholder(tf.float32)
ty = tf.placeholder(tf.float32)

tw = tf.Variable(0.)
tb = tf.Variable(0.)

ty_hat = tf.add(tf.multiply(tw, tx), tb)
loss = tf.reduce_mean(tf.square(ty - ty_hat))
optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.1)
training_step = optimizer.minimize(loss)

init_op = tf.global_variables_initializer()
with tf.Session() as sess:
  sess.run(init_op)

  for i in range(500):
    total_loss = 0.
    print('-'*20)
    print('Iteration ', i)
    print('-'*20)
    print('w = ', sess.run(tw))
    print('b = ', sess.run(tb))

    _, total_loss = sess.run([training_step, loss], feed_dict={tx:x, ty:y})
    total_loss = np.sum(total_loss)
    print('Total loss = ', total_loss, ' at iteration ', i)

  tw, tb = sess.run([tw, tb])

print('Final answer , w = {}, b = {}'.format(tw, tb))
