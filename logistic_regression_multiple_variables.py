import numpy as np
import tensorflow as tf

def generate_data():
  X = np.random.uniform(0, 1, size=[10, 2])
  W = np.array([[2.], [3.5]])
  b = 8.0
  Y = np.matmul(X, W) + b
  return X, Y

def main():
  X, Y = generate_data()

  W = tf.Variable([ [0.5], [0.5]], dtype=np.float64)
  b = tf.Variable(0.5, dtype=np.float64)

  X_ph = tf.placeholder(tf.float64, shape=(10, 2))
  Y_ph = tf.placeholder(tf.float64, shape=(10, 1))

  Y_hat = tf.add(tf.matmul(X_ph, W), b)
  loss = tf.reduce_mean(tf.square(Y_hat-Y_ph))

  optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.1)
  training_step = optimizer.minimize(loss)

  loss_over_time = list()
  init_op = tf.global_variables_initializer()
  with tf.Session() as sess:
    sess.run(init_op)
    for _ in range(100):
        _, l = sess.run([training_step, loss], feed_dict={X_ph:X, Y_ph:Y})
        loss_over_time.append(l)
        wr, br = sess.run([W, b])
        print('w = {}, b = {}'.format(wr, br))
        print('-'*10)

if __name__ == '__main__':
  main()
